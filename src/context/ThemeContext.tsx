import { createContext, useContext, useState } from "react";

type Themes = "light" | "dark";

type ThemeCTX = {
  theme: Themes;
  setTheme: (theme: Themes) => void;
};

const ThemeContext = createContext({} as ThemeCTX);

export const useTheme = () => useContext(ThemeContext);

const ThemeProvider = ({ children }: { children: React.ReactNode }) => {
  const [theme, setTheme] = useState<Themes>("light");

  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeProvider;